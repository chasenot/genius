
start:
		@echo "[+] Start All Containers"
		docker compose up -d --build

build:
		docker compose -f build.yml build
		@echo "[+] push to dockerhub"
		docker compose -f build.yml push

stop:
		docker compose down && docker stop $(docker ps -q) || true;
		@echo "[+] Stop All Containers"

vacuum:
		@echo "[+] Clean logs, journalctl, apt"
		sudo apt-get autoclean
		sudo journalctl --vacuum-time=3d
		sudo truncate -s 0 /var/lib/docker/containers/*/*-json.log || true;
		sudo truncate -s 0 /var/log/**/*.log || true;

wipe: stop vacuum
	@echo "[+] stop running container, delete all stopped containers, data, logs"
	cd /srv/ && docker system prune --all --force --volumes;

upgrade:
	@echo "[+] Upgrading control-agent"
	git pull && docker compose down && docker system prune --all --force --volumes && docker compose up -d --build